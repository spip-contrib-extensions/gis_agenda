# gis_agenda

Un plugin facilitant l'utilisation de gis en lien avec agenda.

Le plugin :
- remplace lors de la configuration d'un évènement les champs lieu et adresse par un formulaire de recherche de point gis
- le point gis est lié à l'évènement
- mais les infos sont aussi stockés dans la table évènement, si jamais le plugin gis est désactivé
