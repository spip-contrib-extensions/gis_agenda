<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// G
	'gis_agenda_description' => 'Faciliter l\'interaction entre GIS et Agenda',
	'formidable_slogan' => 'Géolocaliser vos évènements !'
);
