<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// G
	'gis_agenda' => 'Assigner un lieu à votre évènement',
	'gis_agenda_explication' => 'Il est temps d\'assigner un lieu géolocalisé principal à votre évènement «&nbsp;@titre@&nbsp;».',
	'gis_autre' => 'Autre lieu',
	'gis_principal' => 'Lieu principal'
);
