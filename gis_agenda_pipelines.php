<?php

/**
 * Fichier gérant les pipelines du plugin
 *
 * @package SPIP\gis_agenda\Pipelines
**/

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Remplacer dans le formulaire d'édition d'évènement les champs lieu et adresse par un formulaire de recherche gis
 *
 * @param string $flux
 * @return $flux
 */
function gis_agenda_formulaire_fond($flux) {
	if ($flux['args']['form'] === 'editer_evenement') {
		$data = &$flux['data'];

		// Supprimer le lieu, et l'adresse, on verra plus tard comment mettre directement le formulaire de recherche
		$data = preg_replace('#<div class="editer editer_lieu".*<\/div>#Us', '', $data);//En espérant que le markup ne change pas
		$data = preg_replace('#<div class="editer editer_adresse".*<\/div>#Us', '', $data);//En espérant que le markup ne change pas

	}
	return $flux;
}


/**
 * Rediriger après l'édition d'un évènement vers une page de gestion des liens gis.
 *
 * @param string $flux
 * @return $flux
 */
function gis_agenda_formulaire_traiter($flux) {
	if ($flux['args']['form'] === 'editer_evenement') {
		$origin_redirect = $flux['data']['redirect'] ?? '';
		$id_evenement = $flux['data']['id_evenement'];
		// Pas de redirect explicite > on recherche le exec
		if (!$origin_redirect) {
			$origin_redirect = generer_url_ecrire('evenement', "id_evenement=$id_evenement");
		}
		$origin_redirect_64 = base64_encode($origin_redirect);
		include_spip('action/editer_liens');
		$liens = objet_trouver_liens(
			array('gis' => '*'),
			array('evenements' => $id_evenement),
			array('role' => 'principal')
		);
		if (empty($liens)) {
			$flux['data']['origin_redirect'] = $origin_redirect;
			$flux['data']['redirect'] = generer_url_ecrire('gis_agenda',
			"id_evenement=$id_evenement&redirect=$origin_redirect_64");
		}
	}
	if ($flux['args']['form'] === 'editer_liens'
		and $flux['args']['args'][0] === 'gis'
		and $flux['args']['args'][1] === 'evenement'
		and $flux['args']['args'][3] === 'oui'
		and $redirect = _request('redirect')
		and _request('ajouter_lien')
	) {
		$redirect = base64_decode($redirect);
		$flux['data']['origin_redirect'] = $origin_redirect_64;
		$flux['data']['redirect'] = $redirect;
	}
	return $flux;
}

/**
 * Ajouter un role "principal" sur la liaison gis/evenement
 **/
function gis_agenda_declarer_tables_objets_sql($tables) {
	array_set_merge($tables, 'spip_gis', array(
		'roles_colonne' => 'role',
		'roles_titres' => array(
			'principal'  => 'gis_agenda:gis_principal',
			'autre'  => 'gis_agenda:gis_autre'
		),
		'roles_objets' => array(
			'evenements' => array(
				'choix' => array('principal', 'autre'),
				'defaut' => 'principal'
			)
		)
	));
	return $tables;
}

/**
 * Ajouter la table de roles sur la liaison gis
**/
function gis_agenda_declarer_tables_auxiliaires($tables) {
	$tables['spip_gis_liens']['field']['role'] = 'varchar(30) NOT NULL DEFAULT \'\'';
	$tables['spip_gis_liens']['key']['PRIMARY KEY'] = 'id_gis,id_objet,objet,role';
	return $tables;
}

/**
 * S'assurer qu'il n'y a qu'un lieu principal
**/
function gis_agenda_pre_edition_lien($flux) {
	$args = $flux['args'];
	if ($args['action'] === 'insert'
		and $args['objet_source'] === 'gis'
		and $args['objet'] === 'evenement'
		and $args['role'] === 'principal'
		and (
			$args['action'] === 'insert'
			or
			$args['action'] === 'modifier'
		)
	) {
		// Rechercher role principal existant
		$liaison = objet_trouver_liens(
			array('gis' => '*'),
			array('evenements' => $args['id_objet']),
			array('role' => 'principal')
		);
		if (!empty($liaison)) {
			unset($flux['data']);
		}
	}
	return $flux;
}

/**
 * Si un évènement gis est modifié, actualiser les infos en base dans spip_evenements
 * @param array $flux
 * @return array $flux
*/
function gis_agenda_post_edition($flux) {
	include_spip('base/abstract_sql');
	$args = $flux['args'];
	if (isset($args['type']) and $args['type'] === 'gis') {
		include_spip('action/editer_liens');
		$id_gis = $args['id_objet'];
		$evenements_gis = objet_trouver_liens(array('gis'=>  $id_gis), array('evenements' => '*'), array('role' => 'principal'));
		foreach ($evenements_gis as $evenement) {
			$id_evenement = $evenement['id_objet'];
			gis_agenda_localiser_evenement_depuis_gis($id_evenement, $id_gis);
		}
	}
	return $flux;
}

/**
 * Modifier les évènements pour mettre dedans le lieu et adresse depuis les liens gis
 * @param string|int $id_evenement
 * @param string|int $id_gis
**/
function gis_agenda_localiser_evenement_depuis_gis($id_evenement, $id_gis) {
	include_spip('base/abstract_sql');
	include_spip('action/editer_objet');
	$champs_gis = sql_fetsel('*','spip_gis',"id_gis=$id_gis");
	$champs_evenement = pipeline('gis_agenda_mapper_champs',
		array(
			'args' => array('champs_gis' => $champs_gis),
			'data' => array('lieu' => '', 'adresse' => '')
		)
	);
	objet_modifier('evenement', $id_evenement, $champs_evenement);
}

/**
 * Ecrire dans spip_evenements le lieu principal, si jamais on désactive le plugin
**/
function gis_agenda_post_edition_lien($flux) {
	$args = $flux['args'];
	if (
		$args['objet_source'] === 'gis'
		and $args['objet'] === 'evenement'
		and $args['role'] === 'principal'
		and (
			$args['action'] === 'insert'
			or
			$args['action'] === 'delete'
			or
			$args['action'] === 'supprimer'
		)
	) {
		$id_evenement = $flux['args']['id_objet'];
		$id_gis = $flux['args']['id_objet_source'];
		include_spip('action/editer_objet');
		if ($args['action'] === 'insert') {
			gis_agenda_localiser_evenement_depuis_gis($id_evenement, $id_gis);
		} else {
			objet_modifier('evenement', $id_evenement, array('lieu' => '','adresse' => ''));
		}
	}
	return $flux;
}


/**
 * Dire comment on traduit les champs "gis" en champ "agenda"
 * @param array $flux
 *		- ['args']['champs_gis'] les champs gis
 *		- ['data']['lieu'] le champ lieu pour les évènements
 *		- ['data']['adresse'] le champ adresse pour l'évènement
 * @return array $flux
**/
function gis_agenda_gis_agenda_mapper_champs($flux) {
	include_spip('inc/utils');
	$gis = $flux['args']['champs_gis'];
	$flux['data']['lieu'] = $gis['titre'];
	$champs_utiles =  array('adresse', 'code_postal', 'ville', 'pays');
	$champs_gis_ordonnes = array();
	foreach ($champs_utiles as $champ) {
		$champs_gis_ordonnes[$champ] = $gis[$champ];
	}
	$champs_gis_ordonnes = array_filter($champs_gis_ordonnes);
	$flux['data']['adresse'] = implode(', ', $champs_gis_ordonnes);
	return $flux;
}
