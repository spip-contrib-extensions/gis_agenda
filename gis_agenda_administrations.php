<?php

/**
 * Fichier gérant l'installation et désinstallation du plugin
 *
 * @package SPIP\gis_agenda\Installation
**/

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

include_spip('base/upgrade');
include_spip('inc/config');
/**
 * Installation/maj des tables de gis_agenda
 *
 * @param string $nom_meta_base_version
 *     Nom de la meta informant de la version du schéma de données du plugin installé dans SPIP
 * @param string $version_cible
 *     Version du schéma de données dans ce plugin (déclaré dans paquet.xml)
 * @return void
 */
function gis_agenda_upgrade($nom_meta_base_version, $version_cible) {
	// Création des tables
	$maj = array();
	$maj['create'] = array(
		// Config de gis
		array('ecrire_config', 'gis/geocoder', 'on'),
		array('ecrire_config', 'gis/adresse', 'on'),
		array('ecrire_config', 'gis/gis_objets', array_merge(lire_config('gis/gis_objets') ? lire_config('gis/gis_objets') : array(),array('spip_evenements'))),

		// Config des roles
		array('sql_alter', 'TABLE spip_gis_liens DROP PRIMARY KEY'),
		array('maj_tables', array('spip_gis_liens')),
		array('sql_alter', 'TABLE spip_gis_liens ADD PRIMARY KEY (id_gis,id_objet,objet,role)'),
		array('sql_updateq', 'spip_gis_liens', array('role' => 'autre'), array(
			'role=' . sql_quote(''),
			'objet=' . sql_quote('evenement')
		))
	);
	maj_plugin($nom_meta_base_version, $version_cible, $maj);
}


/**
 * Désinstallation/suppression des tables de gis_agenda
 * On ne supprime pas les roles, car on sait jamais, d'autres plugins pourraient utiliser ces roles.
 * A voir, dans le futur.
 * @param string $nom_meta_base_version
 *     Nom de la meta informant de la version du schéma de données du plugin installé dans SPIP
 * @return void
 */
function gis_agenda_vider_tables($nom_meta_base_version) {
	include_spip('inc/meta');
	effacer_meta($nom_meta_base_version);
}

